@extends('layouts.app')

@section('content')
  <div class="container">
      <div class="row">
          <div class="col-sm-2">
              <img style="width:150px;" src="{{url('').'/'.$package->image}}">
          </div>
          <div class="col-sm-6">
            <h4><b>{{$package->package_name}}</b></h4>
            <h5>{{$package->sub_heading}}</h5>
          </div>
      </div>

      <div class="tabs mt-5">
          <section class="mx-2 pb-3">
          <ul class="nav nav-tabs nav-justified md-tabs indigo" id="myTabJust" role="tablist">
            <li class="nav-item waves-effect waves-light">
              <a class="nav-link active show" id="about-tab-just" data-toggle="tab" href="#about-just" role="tab" aria-controls="about-just" aria-selected="false"><span class="mr-2"><i class="fa fa-info-circle"></i></span>About</a>
            </li>
            <li class="nav-item waves-effect waves-light">
              <a class="nav-link" id="services-tab-just" data-toggle="tab" href="#services-just" role="tab" aria-controls="services-just" aria-selected="false"><span class="mr-2"><i class="fa fa-cubes"></i></span>Services</a>
            </li>
            <li class="nav-item waves-effect waves-light">
              <a class="nav-link" id="message-tab-just" data-toggle="tab" href="#message-just" role="tab" aria-controls="message-just" aria-selected="false"><span class="mr-2"><i class="fa fa-envelope"></i></span>Message</a>
            </li>
            <li class="nav-item waves-effect waves-light">
              <a class="nav-link" id="rating-tab-just" data-toggle="tab" href="#rating-just" role="tab" aria-controls="rating-just" aria-selected="false"><span class="mr-2"><i class="fa fa-star"></i></span>Rating</a>
            </li>
          </ul>
        </section>
      </div>
  </div>

  <div style="background:#f2f2f2;" class="tabs_content">
    <div class="container">
      <div class="tab-content tab-card pt-5" id="myTabContentJust">
        <div class="tab-pane fade active show" id="about-just" role="tabpanel" aria-labelledby="about-tab-just">
          {!! $package->description !!}
        </div>
        <div class="tab-pane fade" id="services-just" role="tabpanel" aria-labelledby="services-tab-just">
              <div class="media-container row ml-5">

                @foreach ($services as $key)
                  <div class="ml-5 mb-5">
                    <div class="card">
                        <div class="card-wrapper mb-3">
                            <div class="card-img">
                                <img style="max-width:200px;height:150px;border-radius:5px;padding:10px" src="{{url('').'/'.$key->image}}" alt="Mobirise" media-simple="true">
                            </div>
                            <div class="card-box">
                                <p style="height:70px" class="mt-3 text-center">
                                    {{$key->service_name}}
                                </p>
                                <p class="text-center text-success"><span>Price: ৳ {{number_format($key->price,2)}}</span></p>

                            </div>
                            <center><button id="btn_cart" style="background: linear-gradient(45deg, rgb(0, 198, 255), rgb(0, 114, 255)) !important;" type="button" class="btn btn-primary" name="button" onclick="add_cart({{$key->id}})">Add to cart</button></center>
                        </div>
                    </div>
                  </div>
                  @endforeach
              </div>
        </div>


        <div class="tab-pane fade" id="message-just" role="tabpanel" aria-labelledby="message-tab-just">
          <div class="card">
            <div class="card-body p-5">
              <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-6">
                    <form action="" method="post">

                      <legend class="text-center">Contact us</legend>

                      <!-- Name input-->
                      <div class="form-group">
                        <label class="col-md-3 control-label" for="name">Name</label>
                        <div class="col-md-9">
                          <input id="name" name="name" type="text" placeholder="Your name" class="form-control">
                        </div>
                      </div>

                      <!-- Email input-->
                      <div class="form-group">
                        <label class="col-md-3 control-label" for="email">Your E-mail</label>
                        <div class="col-md-9">
                          <input id="email" name="email" type="text" placeholder="Your email" class="form-control">
                        </div>
                      </div>

                      <!-- Message body -->
                      <div class="form-group">
                        <label class="col-md-3 control-label" for="message">Your message</label>
                        <div class="col-md-9">
                          <textarea class="form-control" id="message" name="message" placeholder="Please enter your message here..." rows="5"></textarea>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-9 ">
                          <button type="submit" class="btn btn-primary btn-lg ">Submit</button>
                        </div>
                      </div>

                    </form>
                </div>

          	</div>
            </div>
          </div>
        </div>

        <div class="tab-pane fade" id="rating-just" role="tabpanel" aria-labelledby="rating-tab-just">

        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    function add_cart(id)
    {
      $.ajax({
          method: "GET",
          url: "{{url('add_cart?services_id=')}}" + id,
        })
        .done(function( data ) {
          $("#btn_cart").html('Add to cart <i class="fas fa-check"></i>');
        });
      }
  </script>

@endsection
