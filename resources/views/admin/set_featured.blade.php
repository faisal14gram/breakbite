<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <div class='panel-heading'>Edit Sports</div>
    <div class='panel-body'>
      <form method='post' action='{{CRUDBooster::mainpath('edit-save/')}}'>
      @csrf
      <label class="col-sm-2">
          Select Featured
          <span class="text-danger" title="This field is required">*</span>
      </label>


      <div class="col-sm-10">
          @foreach($packages as $key)
            @foreach ($featured as $row)
              @if ($row->packages_id == $key->id)
                <?php $found = 1; ?>
              @endif
            @endforeach

            @if ($found == 1)
              <div class="custom-control custom-checkbox">
                  <input type="checkbox" name="categories[]" class="custom-control-input" value="{{$key->id}}" checked>
                  <label class="custom-control-label" for="defaultUnchecked">{{$key->package_name}} </label>
              </div>
            @else
              <div class="custom-control custom-checkbox">
                  <input type="checkbox" name="categories[]" class="custom-control-input" value="{{$key->id}}">
                  <label class="custom-control-label" for="defaultUnchecked">{{$key->package_name}} </label>
              </div>
            @endif
          @endforeach
      </div>
    </div>


        <div class='panel-footer'>
            <input type='submit' class='btn btn-primary' value='Save changes'/>
        </div>
      </form>
    </div>
  </div>
@endsection
