@extends('layouts.app')

@section('content')

  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <div class="container">

    <div class="card">
      <center><h4 class="mt-3"><b>Cart</b></h4></center>
      <div class="card-body">

        @if ($count==0)

            <center><h4>Your Cart is Empty</h4></center>

          @else

            <table id="cart" class="table table-hover table-condensed">
              				<thead>
          						<tr>
          							<th style="width:50%">Service</th>
          							<th style="width:10%">Price</th>
          							<th style="width:22%" class="text-center">Subtotal</th>
          							<th style="width:10%"></th>
          						</tr>
          					</thead>
          					<tbody>
                      <?php $total = 0; ?>
                      @foreach ($cart as $service)
                        <tr>
            							<td data-th="Product">
            								<div class="row">
            									<div class="col-sm-4 hidden-xs"><img src="{{url('/').'/'.$service->image}}" style="width:50px;height:50px;" alt="..." class="img-responsive"/></div>
            									<div class="col-sm-8">
            										<span>{{$service->service_name}}</span>
            								</div>
                          </td>

                            <td data-th="Price">৳ {{$service->price}}</td>
                            <?php $sub_total = $service->price;?>

                          <?php  $total = $total +  $sub_total;?>
            							<td data-th="Subtotal" class="text-center">৳ {{$sub_total}}</td>
            							<td>
            								<button onclick="cart_delete({{$service->id}})" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
            							</td>
            						</tr>

                      @endforeach
          					</tbody>
          					<tfoot>
          						<tr>
          							<td><a href="#" class="btn btn-warning"><i class="fa fa-angle-left"></i>Go Back</a></td>
          							<td class="hidden-xs"></td>
                      <td class="hidden-xs text-center"><strong>Total ৳ {{$total}}</strong></td>
          							<td><a data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></a></td>
          						</tr>
          					</tfoot>
          				</table>


                  <!-- Collapsible element -->
              <div class="collapse" id="collapseExample">
                <div class="mt-3">
                  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
                    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
                    <div class="container">

                        <div class="col-sm-10">
                          <div class="paymentCont">
						<div class="headingWrap">
								<h3 >Select a Payment Method</h3>
						</div>
						<div class="paymentWrap">
							<div class="btn-group paymentBtnGroup btn-group-justified" data-toggle="buttons">
					             <label class="btn paymentMethod">
				             		<div class="method rocket"></div>
					                <input type="radio" name="options">
					            </label>
					            <label class="btn paymentMethod">
				            		<div class="method cod"></div>
					                <input type="radio" name="options">
					            </label>

					        </div>
						</div>
					</div>
        </div>

<!-- Modal -->
<form action="{{url('service/location/store')}}" method="post">
  @csrf
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Submit Service Location</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-5">
            <div class="form-group">
              <label><b>Name</b></label>
              <input type="text" placeholder="Enter your name" name="name" class="form-control" value="">
            </div>

            <div class="form-group">
              <label><b>Phone Number</b></label>
              <input type="text" placeholder="Enter Phone Number" name="phone" class="form-control" value="">
            </div>
          </div>

          <div class="col-sm-7">

            <div class="form-group">
              <label><b>City</b></label>
                    <select class="form-control" name="city">
                      @foreach($cities as $city)
                      <option value="{{$city->id}}">{{$city->name}}</option>
                      @endforeach
                    </select>     
            </div>

            <div class="form-group">
              <label><b>Area</b></label>
              <select class="form-control" name="area">
                  @foreach($areas as $area)
                    <option value="{{$area->id}}">{{$area->name}}</option>
                  @endforeach
              </select> 
            </div>

            <div class="form-group">
              <label><b>Street Address</b></label>
              <input type="text" placeholder="" name="address" class="form-control" value="">
            </div>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-info" value='Save changes'>
        </div>
    </div>
  </div>
</div>
</form>

                        <div class="row">
                          <div class="col-sm-10">

                          </div>
                          <div class="col-sm-2">
                            <button data-toggle="modal" data-target="#exampleModalCenter" class="btn btn-warning">Place Order</button>
                          </div>

                        </div>

                </div>
              </div>
              <!-- / Collapsible element -->
          </div>

        @endif


    </div>

  </div>

  <script type="text/javascript">
      function cart_delete(id)
      {
        $.ajax({
             type:'get',
             url:'{{url('/cart/delete')}}',
             data:{'item':id},
             success:function(data){
               $('#body').load('{{url('cart')}}');
             }
           });
      }
  </script>
@endsection
