@extends('layouts.app')

@section('content')
  <div class="container">
    <h6><b>Bussiness Category: {{$category->category_name}}</b></h6>

    <div class="packages mt-5">
      @foreach ($packages as $key)
        <div class="card mb-4">
          <div class="card-header">
            <div class="row">
              <div style="max-width:11.667% !important" class="col-sm-2">
                <img style="width:100px" src="{{url('').'/'.$key->image}}" alt="">
              </div>
              <div class="col-sm-6">
                <h6>{{$key->package_name}}</h6>
              </div>
            </div>

          </div>
          <div class="card-body">
            {!! $key->description !!}

            <a class="text-warning" href="{{url('package-details').'/'.$key->id}}"><b>Read more</b></a>
          </div>
          <div class="card-footer">
            <i class="fa fa-map-marker" aria-hidden="true"></i> Dhaka
          </div>
        </div>
      @endforeach
    </div>
  </div>
@endsection
