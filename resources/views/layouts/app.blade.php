<!DOCTYPE html>

<?php use Illuminate\Support\Facades\Auth;

    $id = Auth::Id();
    $cart_count = DB::table('cart')
                ->where('users_id',$id)
                ->count();
    $slider = DB::table('slider')
              ->get();
 ?>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewpor+t" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <style media="screen">
    .categories
    {
      opacity: 0.7;
    }
    .categories:hover
    {
      opacity: 1;
      transition: 0.4s;
      border-radius:5px;
      box-shadow: -2px 3px 60px -14px rgba(104,219,65,1);
    }

    #editor {
    	resize: vertical;
    	overflow: auto;
    	line-height: 1.5;
    	background-color: #fafafa;
      background-image: none;
    	border: 0;
    	min-height: 150px;
    	box-shadow: none;
    	padding: 8px 16px;
    	margin: 0 auto;
    	font-size: 14px;
    	transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
    }
    	#editor:focus {
    		background-color: #f0f0f0;
    		border-color: #38af5b;
    		box-shadow: none;
    		outline: 0 none;
    	}




      .paymentWrap {
  padding: 50px;
  }

  .paymentWrap .paymentBtnGroup {
  max-width: 800px;
  margin: auto;
  }

  .paymentWrap .paymentBtnGroup .paymentMethod {
  padding: 40px;
  box-shadow: none;
  position: relative;
  }

  .paymentWrap .paymentBtnGroup .paymentMethod.active {
  outline: none !important;
  }

  .paymentWrap .paymentBtnGroup .paymentMethod.active .method {
  border-color: #4cd264;
  outline: none !important;
  box-shadow: 0px 3px 22px 0px #7b7b7b;
  }

  .paymentWrap .paymentBtnGroup .paymentMethod .method {
  position: absolute;
  right: 3px;
  top: 3px;
  bottom: 3px;
  left: 3px;
  background-size: contain;
  background-position: center;
  background-repeat: no-repeat;
  border: 2px solid transparent;
  transition: all 0.5s;
  }

  .paymentWrap .paymentBtnGroup .paymentMethod .method.visa {
  background-image: url('images/visa.png');
  background-size: 60px 60px;
  }

  .paymentWrap .paymentBtnGroup .paymentMethod .method.master-card {
  background-image: url('images/mc.png');
  background-size: 60px 60px;
  }

  .paymentWrap .paymentBtnGroup .paymentMethod .method.bkash {
  background-image: url('images/bkash.webp');
    background-size: 60px 60px;
  }

  .paymentWrap .paymentBtnGroup .paymentMethod .method.rocket {
  background-image: url('images/rocket.jpg');
  background-size: 60px 60px;
  }

  .paymentWrap .paymentBtnGroup .paymentMethod .method.cod {
  background-image: url('images/cod.jpg');

  }


  .paymentWrap .paymentBtnGroup .paymentMethod .method:hover {
  border-color: #4cd264;
  outline: none !important;
  }



  .table>tbody>tr>td, .table>tfoot>tr>td{
      vertical-align: middle;
  }
  @media screen and (max-width: 600px) {
      table#cart tbody td .form-control{
  		width:20%;
  		display: inline !important;
  	}
  	.actions .btn{
  		width:36%;
  		margin:1.5em 0;
  	}

  	.actions .btn-info{
  		float:left;
  	}
  	.actions .btn-danger{
  		float:right;
  	}

  	table#cart thead { display: none; }
  	table#cart tbody td { display: block; padding: .6rem; min-width:320px;}
  	table#cart tbody tr td:first-child { background: #333; color: #fff; }
  	table#cart tbody td:before {
  		content: attr(data-th); font-weight: bold;
  		display: inline-block; width: 8rem;
  	}



  	table#cart tfoot td{display:block; }
  	table#cart tfoot td .btn{display:block;}


    </style>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title></title>

    <!-- Scripts -->


    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
    <link href="{{ asset('css/lightbox.css') }}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/lightbox.js') }}" defer></script>
    <link rel="stylesheet" href="{{url('css/app.css')}}">
    <link rel="stylesheet" href="{{url('css/app-style.css')}}">


</head>
<body>
  <div id="body">
    <div id="app">
      <nav class="navbar navbar-expand-lg navbar-light navbar-laravel">
        <div class="container">

        <a class="navbar-brand" href="{{url('/')}}">BreakBite</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto">

            <li class="nav-item mr-2">
              <div class="wrap">
                 <div class="search">
                    <input type="text" class="searchTerm" placeholder="What are you looking for?">
                    <button type="submit" class="searchButton">
                      <i class="fa fa-search"></i>
                   </button>
                 </div>
              </div>
            </li>

            <li class="nav-item mr-2">
              <a href="{{url('cart')}}">
                <i style="font-size:20px;" class="fas fa-shopping-cart"></i>
                  <span style="color:tomato" class="label">{{$cart_count}}</span>
              </a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="#">My Orders<span class="sr-only"></span></a>
            </li>
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                   
          </ul>
        </div>
        </div>
        </nav>


        <div class="slider">
          <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">

            <div class="carousel-inner">
              <div class="carousel-item active">
                <img style="height:335px" class="d-block w-100" src={{url($slider['0']->image)}}>
              </div>

              <?php $i = 0; ?>
              @foreach ($slider as $img)
                @if ($i>0)
                  <div class="carousel-item">
                    <img style="height:335px" class="d-block w-100" src={{url($img->image)}}>
                  </div>
                @endif
                <?php $i++; ?>
            @endforeach
            </div>


            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>


        </div>

        <nav style="background:linear-gradient(45deg, rgb(0, 198, 255), rgb(0, 114, 255)) !important;padding:20px" class="navbar navbar-icon-top navbar-expand-lg">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="container">
          <div class="collapse navbar-collapse" id="navbarSupportedContent1">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                  <a class="nav-link text-white" href="{{url('/')}}">Home</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link text-white" href="#">All Categories</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link text-white" href="#">All Services</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link text-white" href="#">Contact us</a>
                </li>
            </ul>
          </div>
        </div>
      </nav>
        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <footer style="color:#d1a1a1" class="footer bg-dark mt-5 p-5">
      <div class="container" >
        <div class="row" >
          <div class="col-md-10 col-md-offset-1">
            <div class="row">
              <div style="border-right:1px solid gray" class="col-md-2 col-sm-4 col-xs-12">
                <p>
                  <span>
                    <span>Navigate</span>
                  </span>
                </p>
                <ul>
                  <li>
                    <a href="/about">
                    <span>
                      <span>About us</span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span>
                      <span>Contact us</span>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    <span >
                      <span>Developers</span>
                    </span>
                  </a>
                </li>
              </ul>
            </div>
            <div style="border-right:1px solid gray" class="col-md-3 col-sm-4 col-xs-12">
              <p>
                <span>
                  <span>Useful sites</span>
                </span>
              </p>
              <ul>
                <li>
                  <a target="_blank" href="#">Link</a>
                </li>
                <li>
                  <a target="_blank" href="#">Link</a>
                </li>
                <li>
                  <a target="_blank" href="#">Link</a>
                </li>
                <li>
                  <a target="_blank" href="#">Link</a>
                </li>
              </ul>
            </div>
            <div style="border-right:1px solid gray" class="col-md-3 col-sm-4 col-xs-12">
              <p>
                <span>
                  <span>Other links</span>
                </span>
              </p>
              <ul>
                <li>
                  <a href="#">Link</a>
                </li>
                <li>
                  <a href="#">Link</a>
                </li>
                <li>
                  <a href="#">Link</a>
                </li>
              </ul>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
              <p>
                <span>
                  <span >This Site is currectly under constraction</span>
                </span>
              </p>
              <p>
                © 2019 Shakil Anwar
                <span>
                  <span>All Rights Reserved</span>
                </span>
              </p>
            </div>
          </div>
        </div>
    </div>
  </div>
</footer>
</div>
</body>
</html>
