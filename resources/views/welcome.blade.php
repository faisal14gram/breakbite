@extends('layouts.app')
@section('content')

    <div style="background:#efefef" class="content p-3 mt-5">
      <div class="container">

        <div class="media-container row">
          @foreach ($featured as $key)
            <div class="card p-3 col-sm-3">
                <div class="card-wrapper">
                  <h5 class="card-title text-center">
                      <b>{{$key->package_name}}</b>
                  </h5>
                    <div class="card-img">
                        <img style="width:250px;height:200px;border-radius:5px" src="{{$key->image}}" alt="Mobirise" media-simple="true">
                    </div>
                    <div class="card-box">

                        <p style="height:80px" class="mt-3">
                            {{$key->sub_heading}}
                        </p>
                    </div>
                    <div class="mbr-section-btn text-center">
                        <a style="font-weight:500;padding:0.5rem 3rem;" href="{{url('package-details').'/'.$key->id}}" class="btn btn-primary display-4">
                            Learn More
                        </a>
                    </div>
                </div>
            </div>
          @endforeach
        </div>
      </div>
  </div>

    <div class="mt-4">
      <h4 class="text-center">All Categories</h4>
      <div class="m-5">
        <div class="row">
          @foreach ($categories as $key)
            <div class="col-sm-2 mt-3">
              <a class="category-link" href="{{url('packages').'/'.$key->id}}">
                <div class="card card-category">
                  <div style="border-left:4px solid {{$key->color}}" class="card-body">
                    <div class="row">
                      <div class="col-sm-3">
                        <div style="color:{{$key->color}}">{!! $key->image !!}</div>
                      </div>
                      <div class="col-sm-9">
                        <p class="card-text">{{$key->category_name}}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          @endforeach
        </div>
      </div>

    </div>



    <script type="text/javascript">

      $(document).ready(function(){
        $('.category').click(function(){
          var category = $('#category_id').val();

          $.ajax({
           type:'get',
           url:'{{url('sub_category')}}',
           data:{'category_id':category},
           success:function(data){
             $('#sub_categories').html(data);
           }
        });
      });
    });

    </script>

    <script type="text/javascript">
    $('.carousel.carousel-multi-item.v-2 .carousel-item').each(function(){
    var next = $(this).next();
    if (!next.length) {
      next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));

    for (var i=0;i<4;i++) {
      next=next.next();
      if (!next.length) {
        next=$(this).siblings(':first');
      }
      next.children(':first-child').clone().appendTo($(this));
    }
    });
    </script>

    <script type="text/javascript">
      $(document).ready(function () {
      var itemsMainDiv = ('.MultiCarousel');
      var itemsDiv = ('.MultiCarousel-inner');
      var itemWidth = "";

      $('.leftLst, .rightLst').click(function () {
          var condition = $(this).hasClass("leftLst");
          if (condition)
              click(0, this);
          else
              click(1, this)
      });

      ResCarouselSize();




      $(window).resize(function () {
          ResCarouselSize();
      });

  //this function define the size of the items
  function ResCarouselSize() {
      var incno = 0;
      var dataItems = ("data-items");
      var itemClass = ('.item');
      var id = 0;
      var btnParentSb = '';
      var itemsSplit = '';
      var sampwidth = $(itemsMainDiv).width();
      var bodyWidth = $('body').width();
      $(itemsDiv).each(function () {
          id = id + 1;
          var itemNumbers = $(this).find(itemClass).length;
          btnParentSb = $(this).parent().attr(dataItems);
          itemsSplit = btnParentSb.split(',');
          $(this).parent().attr("id", "MultiCarousel" + id);


          if (bodyWidth >= 1200) {
              incno = itemsSplit[3];
              itemWidth = sampwidth / incno;
          }
          else if (bodyWidth >= 992) {
              incno = itemsSplit[2];
              itemWidth = sampwidth / incno;
          }
          else if (bodyWidth >= 768) {
              incno = itemsSplit[1];
              itemWidth = sampwidth / incno;
          }
          else {
              incno = itemsSplit[0];
              itemWidth = sampwidth / incno;
          }
          $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
          $(this).find(itemClass).each(function () {
              $(this).outerWidth(itemWidth);
          });

          $(".leftLst").addClass("over");
          $(".rightLst").removeClass("over");

      });
  }


  //this function used to move the items
      function ResCarousel(e, el, s) {
          var leftBtn = ('.leftLst');
          var rightBtn = ('.rightLst');
          var translateXval = '';
          var divStyle = $(el + ' ' + itemsDiv).css('transform');
          var values = divStyle.match(/-?[\d\.]+/g);
          var xds = Math.abs(values[4]);
          if (e == 0) {
              translateXval = parseInt(xds) - parseInt(itemWidth * s);
              $(el + ' ' + rightBtn).removeClass("over");

              if (translateXval <= itemWidth / 2) {
                  translateXval = 0;
                  $(el + ' ' + leftBtn).addClass("over");
              }
          }
          else if (e == 1) {
              var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
              translateXval = parseInt(xds) + parseInt(itemWidth * s);
              $(el + ' ' + leftBtn).removeClass("over");

              if (translateXval >= itemsCondition - itemWidth / 2) {
                  translateXval = itemsCondition;
                  $(el + ' ' + rightBtn).addClass("over");
              }
          }
          $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
      }

      //It is used to get some elements from btn
      function click(ell, ee) {
          var Parent = "#" + $(ee).parent().attr("id");
          var slide = $(Parent).attr("data-slide");
          ResCarousel(ell, Parent, slide);
      }

    });
  </script>



@endsection
