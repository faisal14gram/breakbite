-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 26, 2019 at 01:58 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `breakbite`
--

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

CREATE TABLE `areas` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `areas`
--

INSERT INTO `areas` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Dhanmondi', '2019-10-17 09:22:07', NULL),
(2, 'Sukrabad', '2019-10-17 09:22:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `services_id` int(11) DEFAULT NULL,
  `users_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `created_at`, `updated_at`, `price`, `services_id`, `users_id`) VALUES
(52, NULL, NULL, '120', 1, 1),
(53, NULL, NULL, '120', 1, 3),
(54, NULL, NULL, '120', 1, 3),
(55, NULL, NULL, '120', 1, 1),
(56, NULL, NULL, '120', 1, 4),
(57, NULL, NULL, '120', 1, 4),
(58, NULL, NULL, '120', 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `created_at`, `updated_at`, `category_name`, `image`, `color`) VALUES
(1, '2019-09-07 22:28:41', '2019-09-07 22:54:33', 'Cleaning', '<i class=\"fas fa-broom\"></i>', '#ffcc00'),
(2, '2019-09-10 03:57:01', NULL, 'Child Care', '<i class=\"fas fa-baby\"></i>', '#00cc66'),
(3, '2019-09-10 03:59:44', NULL, 'Gardening', '<i class=\"fas fa-tree\"></i>', '#33cc33'),
(4, '2019-09-10 04:01:06', NULL, 'Paint', '<i class=\"fas fa-paint-roller\"></i>', '#ff9933'),
(5, '2019-09-10 04:01:57', NULL, 'Plumbing', '<i class=\"fas fa-wrench\"></i>', '#ccff66'),
(6, '2019-09-10 04:02:55', NULL, 'Moving', '<i class=\"fas fa-suitcase-rolling\"></i>', '#009933');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Dhaka', '2019-10-17 09:25:50', NULL),
(2, 'Chittagong', '2019-10-17 09:26:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_apicustom`
--

CREATE TABLE `cms_apicustom` (
  `id` int(10) UNSIGNED NOT NULL,
  `permalink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tabel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aksi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kolom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderby` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_query_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sql_where` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `method_type` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responses` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_apikey`
--

CREATE TABLE `cms_apikey` (
  `id` int(10) UNSIGNED NOT NULL,
  `screetkey` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_dashboard`
--

CREATE TABLE `cms_dashboard` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_email_queues`
--

CREATE TABLE `cms_email_queues` (
  `id` int(10) UNSIGNED NOT NULL,
  `send_at` datetime DEFAULT NULL,
  `email_recipient` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_attachments` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_sent` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_email_templates`
--

CREATE TABLE `cms_email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_email_templates`
--

INSERT INTO `cms_email_templates` (`id`, `name`, `slug`, `subject`, `content`, `description`, `from_name`, `from_email`, `cc_email`, `created_at`, `updated_at`) VALUES
(1, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2019-05-27 22:11:28', NULL),
(2, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2019-05-27 22:17:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_logs`
--

CREATE TABLE `cms_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ipaddress` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useragent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_users` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_logs`
--

INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-05-27 22:12:29', NULL),
(2, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-05-27 23:01:17', NULL),
(3, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/vendors/add-save', 'Add New Data Shakil\'s Shop at Vendors', '', 1, '2019-05-27 23:24:04', NULL),
(4, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-05-28 06:26:32', NULL),
(5, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-05-28 21:41:54', NULL),
(6, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-05-29 00:07:01', NULL),
(7, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/categories/add-save', 'Add New Data Laptop Servicing at Categories', '', 1, '2019-05-29 00:09:46', NULL),
(8, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/categories/add-save', 'Add New Data Mobile Servicing at Categories', '', 1, '2019-05-29 00:25:14', NULL),
(9, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/categories/add-save', 'Add New Data Computer Servicing at Categories', '', 1, '2019-05-29 00:25:42', NULL),
(10, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/categories/add-save', 'Add New Data Laptop Servicing at Categories', '', 1, '2019-05-29 00:26:01', NULL),
(11, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/categories/add-save', 'Add New Data Mobile Servicing at Categories', '', 1, '2019-05-29 00:26:09', NULL),
(12, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/categories/add-save', 'Add New Data Mobile Servicing at Categories', '', 1, '2019-05-29 00:26:23', NULL),
(13, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/categories/add-save', 'Add New Data Computer Servicing at Categories', '', 1, '2019-05-29 00:26:44', NULL),
(14, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/categories/delete/7', 'Delete data Computer Servicing at Categories', '', 1, '2019-05-29 00:40:09', NULL),
(15, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/categories/delete/6', 'Delete data Mobile Servicing at Categories', '', 1, '2019-05-29 00:40:16', NULL),
(16, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/categories/delete/5', 'Delete data Mobile Servicing at Categories', '', 1, '2019-05-29 00:40:19', NULL),
(17, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/categories/delete/4', 'Delete data Laptop Servicing at Categories', '', 1, '2019-05-29 01:52:30', NULL),
(18, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/categories/add-save', 'Add New Data Laptop Servicing at Categories', '', 1, '2019-05-29 01:52:46', NULL),
(19, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/categories/delete/3', 'Delete data Computer Servicing at Categories', '', 1, '2019-05-29 01:54:06', NULL),
(20, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/categories/delete/2', 'Delete data Mobile Servicing at Categories', '', 1, '2019-05-29 01:54:11', NULL),
(21, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/categories/delete/1', 'Delete data Laptop Servicing at Categories', '', 1, '2019-05-29 01:54:15', NULL),
(22, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/categories/add-save', 'Add New Data Computer Servicing at Categories', '', 1, '2019-05-29 01:55:07', NULL),
(23, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/slider/add-save', 'Add New Data  at HomeSlider', '', 1, '2019-05-29 02:05:29', NULL),
(24, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/slider/add-save', 'Add New Data  at HomeSlider', '', 1, '2019-05-29 02:06:07', NULL),
(25, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/slider/add-save', 'Add New Data  at HomeSlider', '', 1, '2019-05-29 02:15:37', NULL),
(26, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/sub_categories/add-save', 'Add New Data HP Laptop at Sub Categories', '', 1, '2019-05-29 02:34:02', NULL),
(27, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-05-29 03:24:04', NULL),
(28, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/sub_categories/add-save', 'Add New Data Acer Laptop at Sub Categories', '', 1, '2019-05-29 05:02:35', NULL),
(29, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/sub_categories/add-save', 'Add New Data Dell Laptop at Sub Categories', '', 1, '2019-05-29 05:02:47', NULL),
(30, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/sub_categories/add-save', 'Add New Data Asus Laptop at Sub Categories', '', 1, '2019-05-29 05:02:55', NULL),
(31, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/sub_categories/edit-save/3', 'Update data Dell Laptop at Sub Categories', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>image</td><td></td><td>uploads/1/2019-05/1024px_dell_logosvg.png</td></tr></tbody></table>', 1, '2019-05-29 05:21:18', NULL),
(32, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/sub_categories/edit-save/4', 'Update data Asus Laptop at Sub Categories', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>image</td><td></td><td>uploads/1/2019-05/asus_logo.jpg</td></tr></tbody></table>', 1, '2019-05-29 05:34:38', NULL),
(33, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/sub_categories/edit-save/2', 'Update data Acer Laptop at Sub Categories', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>image</td><td></td><td>uploads/1/2019-05/256_256_f37bd5c8c36eeb36ed779054550246f2_acer.png</td></tr></tbody></table>', 1, '2019-05-29 05:34:50', NULL),
(34, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/sub_categories/edit-save/1', 'Update data HP Laptop at Sub Categories', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>image</td><td></td><td>uploads/1/2019-05/1024px_hp_new_logo_2dsvg.png</td></tr></tbody></table>', 1, '2019-05-29 05:35:00', NULL),
(35, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/categories/add-save', 'Add New Data Electric and Sanitary at Categories', '', 1, '2019-05-29 07:38:10', NULL),
(36, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/categories/edit-save/8', 'Update data Cleaning & Pest Control at Categories', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>category_name</td><td>Laptop Servicing</td><td>Cleaning & Pest Control</td></tr><tr><td>image</td><td><i class=\"fas fa-laptop-medical\"></i></td><td><i class=\"fas fa-broom\"></i></td></tr></tbody></table>', 1, '2019-05-29 07:40:31', NULL),
(37, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/sub_categories/delete-image', 'Delete the image of Asus Laptop at Sub Categories', '', 1, '2019-05-29 07:42:26', NULL),
(38, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/sub_categories/edit-save/4', 'Update data Asus Laptop at Sub Categories', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>image</td><td></td><td>uploads/1/2019-05/purple_luxury_eid_mubarak_design_1017_8795_640x640.jpg</td></tr></tbody></table>', 1, '2019-05-29 07:42:33', NULL),
(39, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/sub_categories/edit-save/4', 'Update data Cleaning And EID Package at Sub Categories', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sub_category_name</td><td>Asus Laptop</td><td>Cleaning And EID Package</td></tr></tbody></table>', 1, '2019-05-29 07:43:13', NULL),
(40, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/sub_categories/delete-image', 'Delete the image of Dell Laptop at Sub Categories', '', 1, '2019-05-29 07:44:32', NULL),
(41, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/sub_categories/edit-save/3', 'Update data Professional Home Cleaning at Sub Categories', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sub_category_name</td><td>Dell Laptop</td><td>Professional Home Cleaning</td></tr><tr><td>image</td><td></td><td>uploads/1/2019-05/hijyen.jpg</td></tr></tbody></table>', 1, '2019-05-29 07:44:54', NULL),
(42, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/sub_categories/delete-image', 'Delete the image of Acer Laptop at Sub Categories', '', 1, '2019-05-29 07:47:25', NULL),
(43, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/sub_categories/edit-save/2', 'Update data Tank Cleaning at Sub Categories', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sub_category_name</td><td>Acer Laptop</td><td>Tank Cleaning</td></tr><tr><td>image</td><td></td><td>uploads/1/2019-05/hdpe_water_tank_500x500.jpg</td></tr></tbody></table>', 1, '2019-05-29 07:47:40', NULL),
(44, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/sub_categories/delete-image', 'Delete the image of HP Laptop at Sub Categories', '', 1, '2019-05-29 07:48:23', NULL),
(45, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/sub_categories/edit-save/1', 'Update data Fridge Cleaning at Sub Categories', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sub_category_name</td><td>HP Laptop</td><td>Fridge Cleaning</td></tr><tr><td>image</td><td></td><td>uploads/1/2019-05/cleaning_fridge_today_tease_1_160114_cd17100056604e4ad349068b6624e3d9.jpg</td></tr></tbody></table>', 1, '2019-05-29 07:48:38', NULL),
(46, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/sub_categories/edit-save/4', 'Update data Cleaning EID Package at Sub Categories', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sub_category_name</td><td>Cleaning And EID Package</td><td>Cleaning EID Package</td></tr></tbody></table>', 1, '2019-05-29 07:54:31', NULL),
(47, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://localhost:8000/admin/services/add-save', 'Add New Data Eid Sofa Cleaning at Services', '', 1, '2019-05-29 08:11:41', NULL),
(48, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-05-29 20:11:40', NULL),
(49, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/services/edit-save/1', 'Update data Eid Sofa Cleaning at Services', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-05-29 20:13:38', NULL),
(50, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-05-30 08:00:57', NULL),
(51, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/services/add-save', 'Add New Data Shihab will clean your home at Services', '', 1, '2019-05-30 09:35:04', NULL),
(52, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/services/add-save', 'Add New Data Test Service at Services', '', 1, '2019-05-30 10:21:41', NULL),
(53, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/slider/delete/3', 'Delete data 3 at HomeSlider', '', 1, '2019-05-30 11:55:14', NULL),
(54, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/slider/delete/2', 'Delete data 2 at HomeSlider', '', 1, '2019-05-30 11:55:18', NULL),
(55, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/slider/delete/1', 'Delete data 1 at HomeSlider', '', 1, '2019-05-30 11:55:22', NULL),
(56, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/slider/add-save', 'Add New Data  at HomeSlider', '', 1, '2019-05-30 11:55:40', NULL),
(57, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/slider/add-save', 'Add New Data  at HomeSlider', '', 1, '2019-05-30 11:56:00', NULL),
(58, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/slider/add-save', 'Add New Data  at HomeSlider', '', 1, '2019-05-30 11:56:06', NULL),
(59, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/slider/add-save', 'Add New Data  at HomeSlider', '', 1, '2019-05-30 11:56:11', NULL),
(60, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/slider/delete/5', 'Delete data 5 at HomeSlider', '', 1, '2019-05-30 11:57:01', NULL),
(61, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-05-31 09:47:05', NULL),
(62, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-05-31 22:52:47', NULL),
(63, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-06-02 00:36:33', NULL),
(64, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-06-13 04:02:52', NULL),
(65, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36', 'http://127.0.0.1:8000/admin/services/add-save', 'Add New Data Test Tank cleaning Service at Services', '', 1, '2019-06-13 04:04:33', NULL),
(66, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36', 'http://127.0.0.1:8000/admin/slider/delete/7', 'Delete data 7 at HomeSlider', '', 1, '2019-06-13 04:06:50', NULL),
(67, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36', 'http://127.0.0.1:8000/admin/slider/delete/6', 'Delete data 6 at HomeSlider', '', 1, '2019-06-13 04:06:53', NULL),
(68, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36', 'http://127.0.0.1:8000/admin/slider/add-save', 'Add New Data  at HomeSlider', '', 1, '2019-06-13 04:07:07', NULL),
(69, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36', 'http://127.0.0.1:8000/admin/slider/add-save', 'Add New Data  at HomeSlider', '', 1, '2019-06-13 04:07:14', NULL),
(70, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.90 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-06-16 08:03:37', NULL),
(71, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-06-26 07:58:05', NULL),
(72, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://127.0.0.1:8000/admin/services/add-save', 'Add New Data Nothing at Services', '', 1, '2019-06-26 12:08:39', NULL),
(73, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/login', 'admin@crudbooster.com login with IP Address 103.214.159.110', '', 1, '2019-07-04 14:06:46', NULL),
(74, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/services/delete/5', 'Delete data Nothing at Services', '', 1, '2019-07-04 14:06:55', NULL),
(75, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/services/delete/4', 'Delete data Test Tank cleaning Service at Services', '', 1, '2019-07-04 14:06:58', NULL),
(76, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/services/delete/3', 'Delete data Test Service at Services', '', 1, '2019-07-04 14:07:01', NULL),
(77, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/services/delete/2', 'Delete data Shihab will clean your home at Services', '', 1, '2019-07-04 14:07:04', NULL),
(78, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/services/delete/1', 'Delete data Eid Sofa Cleaning at Services', '', 1, '2019-07-04 14:07:08', NULL),
(79, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/services/add-save', 'Add New Data Professional Home Cleanimg at Services', '', 1, '2019-07-04 14:08:00', NULL),
(80, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/services/add-save', 'Add New Data Kitchen Deep Cleaning at Services', '', 1, '2019-07-04 14:10:20', NULL),
(81, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/services/add-save', 'Add New Data Carpet Cleaning at Services', '', 1, '2019-07-04 14:12:40', NULL),
(82, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/services/add-save', 'Add New Data Chair/Sofa Cleaning at Services', '', 1, '2019-07-04 14:14:01', NULL),
(83, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/categories/add-save', 'Add New Data Appliance Repair at Categories', '', 1, '2019-07-04 14:17:50', NULL),
(84, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/sub_categories/add-save', 'Add New Data AC Service and Repair at Sub Categories', '', 1, '2019-07-04 14:19:36', NULL),
(85, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/sub_categories/add-save', 'Add New Data Washing machine Repair at Sub Categories', '', 1, '2019-07-04 14:20:31', NULL),
(86, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/services/add-save', 'Add New Data Washing Machine check up at Services', '', 1, '2019-07-04 14:22:50', NULL),
(87, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/services/add-save', 'Add New Data Washing machine servicing at Services', '', 1, '2019-07-04 14:23:48', NULL),
(88, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/slider/delete/9', 'Delete data 9 at HomeSlider', '', 1, '2019-07-04 14:26:40', NULL),
(89, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/slider/delete/8', 'Delete data 8 at HomeSlider', '', 1, '2019-07-04 14:26:46', NULL),
(90, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/slider/delete/4', 'Delete data 4 at HomeSlider', '', 1, '2019-07-04 14:26:49', NULL),
(91, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/slider/add-save', 'Add New Data  at HomeSlider', '', 1, '2019-07-04 14:27:08', NULL),
(92, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/slider/add-save', 'Add New Data  at HomeSlider', '', 1, '2019-07-04 14:31:39', NULL),
(93, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/slider/add-save', 'Add New Data  at HomeSlider', '', 1, '2019-07-04 14:32:43', NULL),
(94, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/services/add-save', 'Add New Data Professional fridge cleaning at Services', '', 1, '2019-07-04 14:36:32', NULL),
(95, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/services/add-save', 'Add New Data Septic Tank Cleaning Services at Services', '', 1, '2019-07-04 14:40:24', NULL),
(96, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/services/add-save', 'Add New Data Water Tank Cleaning (Manual Method) at Services', '', 1, '2019-07-04 14:40:59', NULL),
(97, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/sub_categories/add-save', 'Add New Data Laptop Servicing at Sub Categories', '', 1, '2019-07-04 14:43:32', NULL),
(98, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/sub_categories/add-save', 'Add New Data Desktop Servicing at Sub Categories', '', 1, '2019-07-04 14:43:47', NULL),
(99, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/services/add-save', 'Add New Data Desktop Software Installation / Up-gradation Service at Services', '', 1, '2019-07-04 14:45:27', NULL),
(100, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/services/add-save', 'Add New Data Desktop Hardware Related Services at Services', '', 1, '2019-07-04 14:46:03', NULL),
(101, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/services/add-save', 'Add New Data Laptop/Note Book Software Related Solutions at Services', '', 1, '2019-07-04 14:47:33', NULL),
(102, '103.214.159.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'https://sisdemo.club/breakbite/public/admin/services/add-save', 'Add New Data Laptop/Notebook Hardware Related Solutions at Services', '', 1, '2019-07-04 14:48:50', NULL),
(103, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-07-05 03:40:49', NULL),
(104, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/add-save', 'Add New Data Orders at Menu Management', '', 1, '2019-07-05 03:43:17', NULL),
(105, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-07-05 03:52:55', NULL),
(106, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/delete/6', 'Delete data Orders at Menu Management', '', 1, '2019-07-05 04:08:31', NULL),
(107, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/add-save', 'Add New Data Orders at Menu Management', '', 1, '2019-07-05 04:11:32', NULL),
(108, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/7', 'Update data Orders at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>0</td><td></td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-05 04:11:47', NULL),
(109, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/7', 'Update data Orders at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 1, '2019-07-05 04:12:41', NULL),
(110, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://127.0.0.1:8000/admin/users/edit-save/1', 'Update data BreakBite Tech Team at Users Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Super Admin</td><td>BreakBite Tech Team</td></tr><tr><td>photo</td><td></td><td>uploads/1/2019-07/292fd6b59e.png</td></tr><tr><td>email</td><td>admin@crudbooster.com</td><td>admin@breakbite.com</td></tr><tr><td>password</td><td>$2y$10$Hw3oaNRug.sp/ZfaZrVPCOImbCSxEMXYgr06TlTwtLs8gIWiaZ7hu</td><td>$2y$10$P6O2uN6ZWwjQ1mdOppZ6Re8uLJRVfqUB5rxphpoFvBcH1XIMamJky</td></tr><tr><td>id_cms_privileges</td><td>1</td><td></td></tr><tr><td>status</td><td>Active</td><td></td></tr></tbody></table>', 1, '2019-07-05 04:25:25', NULL),
(111, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'admin@breakbite.com logout', '', 1, '2019-07-05 04:26:15', NULL),
(112, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@breakbite.com login with IP Address 127.0.0.1', '', 1, '2019-07-05 04:26:48', NULL),
(113, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'admin@breakbite.com logout', '', 1, '2019-07-05 11:31:20', NULL),
(114, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@breakbite.com login with IP Address 127.0.0.1', '', 1, '2019-07-06 08:46:12', NULL),
(115, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@breakbite.com login with IP Address 127.0.0.1', '', 1, '2019-09-07 10:47:20', NULL),
(116, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/sub_categories/delete/8', 'Delete data Desktop Servicing at Sub Categories', '', 1, '2019-09-07 10:48:17', NULL),
(117, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/sub_categories/action-selected', 'Delete data 7,6,5,4,3,2,1 at Sub Categories', '', 1, '2019-09-07 10:48:25', NULL),
(118, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/categories/action-selected', 'Delete data 11,10,9,8 at Categories', '', 1, '2019-09-07 10:48:35', NULL),
(119, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/services/action-selected', 'Delete data 18,17,16,15,14,13,12,11,10,9,8,7,6 at Services', '', 1, '2019-09-07 10:48:43', NULL),
(120, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/slider/delete-image', 'Delete the image of 12 at HomeSlider', '', 1, '2019-09-07 10:49:32', NULL),
(121, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/slider/edit-save/12', 'Update data  at HomeSlider', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>image</td><td></td><td>uploads/1/2019-09/tool_work_bench_hammer_pliers_53987.jpeg</td></tr></tbody></table>', 1, '2019-09-07 10:53:20', NULL),
(122, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/slider/delete/11', 'Delete data 11 at HomeSlider', '', 1, '2019-09-07 10:53:25', NULL),
(123, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/slider/delete/10', 'Delete data 10 at HomeSlider', '', 1, '2019-09-07 10:53:29', NULL),
(124, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@breakbite.com login with IP Address 127.0.0.1', '', 1, '2019-09-07 22:19:34', NULL),
(125, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/menu_management/delete/1', 'Delete data Vendors at Menu Management', '', 1, '2019-09-07 22:19:57', NULL),
(126, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/menu_management/add-save', 'Add New Data Categories at Menu Management', '', 1, '2019-09-07 22:23:11', NULL),
(127, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/8', 'Update data Categories at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>3</td><td></td></tr></tbody></table>', 1, '2019-09-07 22:23:49', NULL),
(128, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/8', 'Update data Categories at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>path</td><td>AdminCategoriesControllerGetIndex</td><td>AdminCategoriesController@GetIndex</td></tr><tr><td>parent_id</td><td>3</td><td></td></tr></tbody></table>', 1, '2019-09-07 22:24:40', NULL),
(129, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/8', 'Update data Categories at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>type</td><td>Controller & Method</td><td>Module</td></tr><tr><td>path</td><td>AdminCategoriesController@GetIndex</td><td>categories</td></tr><tr><td>parent_id</td><td>3</td><td></td></tr></tbody></table>', 1, '2019-09-07 22:25:01', NULL),
(130, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/categories/add-save', 'Add New Data Cleaning at Categories', '', 1, '2019-09-07 22:28:41', NULL),
(131, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/categories/edit-save/1', 'Update data Cleaning at Categories', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>#ffcc00</td></tr></tbody></table>', 1, '2019-09-07 22:54:33', NULL),
(132, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/menu_management/add-save', 'Add New Data Catalog at Menu Management', '', 1, '2019-09-07 23:55:33', NULL),
(133, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/module_generator/delete/17', 'Delete data Featured at Module Generator', '', 1, '2019-09-07 23:58:29', NULL),
(134, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/menu_management/add-save', 'Add New Data Featured at Menu Management', '', 1, '2019-09-08 00:00:36', NULL),
(135, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/11', 'Update data Featured at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>type</td><td>URL</td><td>Route</td></tr><tr><td>path</td><td>CRUDBooster::mainpath(\'set-featured\')</td><td>admin/set-featured</td></tr><tr><td>parent_id</td><td>10</td><td></td></tr></tbody></table>', 1, '2019-09-08 00:03:12', NULL),
(136, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/11', 'Update data Featured at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>path</td><td>admin/set-featured</td><td>set-featured</td></tr><tr><td>parent_id</td><td>10</td><td></td></tr></tbody></table>', 1, '2019-09-08 00:03:34', NULL),
(137, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/11', 'Update data Featured at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>type</td><td>Route</td><td>Controller & Method</td></tr><tr><td>path</td><td>set-featured</td><td>AdminSubCategoriesController@setFeatured</td></tr><tr><td>parent_id</td><td>10</td><td></td></tr></tbody></table>', 1, '2019-09-08 00:04:03', NULL),
(138, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/sub_categories/add-save', 'Add New Data Something at Sub Categories', '', 1, '2019-09-08 00:20:35', NULL),
(139, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/menu_management/edit-save/5', 'Update data Service Packages at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Sub Categories</td><td>Service Packages</td></tr><tr><td>type</td><td>Route</td><td>Module</td></tr><tr><td>path</td><td>AdminSubCategoriesControllerGetIndex</td><td>service_packages</td></tr><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-list-alt</td><td>fa fa-gift</td></tr><tr><td>parent_id</td><td>3</td><td></td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>', 1, '2019-09-08 00:34:40', NULL),
(140, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/service_packages/add-save', 'Add New Data Plumbing Services at Service Packages', '', 1, '2019-09-08 01:38:52', NULL),
(141, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'http://localhost:8000/admin/service_packages/edit-save/2', 'Update data Plumbing Services at Service Packages', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sub_heading</td><td></td><td>Installing, repairing and maintaining pipes, fixtures and other plumbing</td></tr><tr><td>description</td><td></td><td><p style=\"font-family: Verdana, &quot;Open Sans&quot;, sans-serif; margin-bottom: 24px; overflow-wrap: break-word; color: rgb(96, 96, 96); background-color: rgb(242, 242, 242);\">I can assemble pipe sections, tubing and fittings, using couplings, clamps, screws, bolts, cement, plastic solvent, caulking, or soldering, brazing and welding equipment. I can fill pipes or plumbing fixtures with water or air and observe pressure gauges to detect and locate leaks.</p><p style=\"font-family: Verdana, &quot;Open Sans&quot;, sans-serif; margin-bottom: 24px; overflow-wrap: break-word; color: rgb(96, 96, 96); background-color: rgb(242, 242, 242);\">I can also Review blueprints and building codes and specifications to determine work details and procedures. I can prepare written work cost estimates and negotiate contracts and keep records of assignments and produce detailed work reports.</p></td></tr></tbody></table>', 1, '2019-09-08 06:32:52', NULL),
(142, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/login', 'admin@breakbite.com login with IP Address 127.0.0.1', '', 1, '2019-09-08 06:45:26', NULL),
(143, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/service_packages/delete-image', 'Delete the image of Something at Service Packages', '', 1, '2019-09-08 06:47:20', NULL),
(144, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/service_packages/edit-save/1', 'Update data Something at Service Packages', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>image</td><td></td><td>uploads/1/2019-09/image.jpg</td></tr><tr><td>sub_heading</td><td></td><td>I can assemble pipe sections, tubing and fittings, using couplings, clamps, screws, bolts, cement, plastic solvent, caulking, or soldering, brazing and welding equipment. I can fill pipes or plumbing fixtures with water or air</td></tr><tr><td>description</td><td></td><td><p>esgash<br></p></td></tr></tbody></table>', 1, '2019-09-08 06:48:55', NULL),
(145, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/service_packages/edit-save/1', 'Update data Something at Service Packages', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sub_heading</td><td>I can assemble pipe sections, tubing and fittings, using couplings, clamps, screws, bolts, cement, plastic solvent, caulking, or soldering, brazing and welding equipment. I can fill pipes or plumbing fixtures with water or air</td><td>I can assemble pipe sections, tubing and fittings, using couplings, clamps, screws, bolts, cement, plastic solvent, I can assemble pipe sections,caulking, or soldering, brazing and welding equipment. I can fill pipes or plumbing fixtures with water or air</td></tr></tbody></table>', 1, '2019-09-08 06:50:36', NULL),
(146, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/service_packages/edit-save/1', 'Update data Something at Service Packages', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sub_heading</td><td>I can assemble pipe sections, tubing and fittings, using couplings, clamps, screws, bolts, cement, plastic solvent, I can assemble pipe sections,caulking, or soldering, brazing and welding equipment. I can fill pipes or plumbing fixtures with water or air</td><td>I can assemble pipe sections, tubing and fittings, using couplings, clamps, screws, bolts, cement, plastic solvent, caulking, or soldering, brazing and welding equipment. I can fill pipes or plumbing fixtures with water or air</td></tr></tbody></table>', 1, '2019-09-08 06:52:47', NULL);
INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(147, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/service_packages/edit-save/1', 'Update data Something at Service Packages', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sub_heading</td><td>I can assemble pipe sections, tubing and fittings, using couplings, clamps, screws, bolts, cement, plastic solvent, caulking, or soldering, brazing and welding equipment. I can fill pipes or plumbing fixtures with water or air</td><td>cement, plastic solvent, caulking, or soldering, brazing and welding equipment. I can fill pipes or plumbing fixtures with water or air</td></tr></tbody></table>', 1, '2019-09-08 06:52:59', NULL),
(148, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/login', 'admin@breakbite.com login with IP Address 127.0.0.1', '', 1, '2019-09-08 23:07:55', NULL),
(149, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/services/add-save', 'Add New Data  at Services', '', 1, '2019-09-08 23:15:55', NULL),
(150, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/services/edit-save/1', 'Update data  at Services', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>service_name</td><td></td><td>Planting Avocado</td></tr><tr><td>price_fixed</td><td>0</td><td></td></tr><tr><td>price_range</td><td></td><td></td></tr></tbody></table>', 1, '2019-09-08 23:18:22', NULL),
(151, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/login', 'admin@breakbite.com login with IP Address 127.0.0.1', '', 1, '2019-09-10 03:45:26', NULL),
(152, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/categories/add-save', 'Add New Data Child Care at Categories', '', 1, '2019-09-10 03:57:01', NULL),
(153, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/categories/add-save', 'Add New Data Gardening at Categories', '', 1, '2019-09-10 03:59:44', NULL),
(154, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/categories/add-save', 'Add New Data Paint at Categories', '', 1, '2019-09-10 04:01:06', NULL),
(155, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/categories/add-save', 'Add New Data Plumbing at Categories', '', 1, '2019-09-10 04:01:57', NULL),
(156, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/categories/add-save', 'Add New Data Moving at Categories', '', 1, '2019-09-10 04:02:55', NULL),
(157, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/service_packages/delete-image', 'Delete the image of Plumbing Services at Service Packages', '', 1, '2019-09-10 04:34:41', NULL),
(158, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/service_packages/edit-save/2', 'Update data Plumbing Services at Service Packages', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>categories_id</td><td>1</td><td>5</td></tr><tr><td>image</td><td></td><td>uploads/1/2019-09/plumbing_1433606_960_720.jpg</td></tr></tbody></table>', 1, '2019-09-10 04:35:01', NULL),
(159, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/service_packages/delete-image', 'Delete the image of Something at Service Packages', '', 1, '2019-09-10 04:35:19', NULL),
(160, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/service_packages/edit-save/1', 'Update data Gardening at Service Packages', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>package_name</td><td>Something</td><td>Gardening</td></tr><tr><td>categories_id</td><td>1</td><td>3</td></tr><tr><td>image</td><td></td><td>uploads/1/2019-09/salad_water_garden_plant.jpg</td></tr><tr><td>description</td><td><p>esgash<br></p></td><td><p><br></p></td></tr></tbody></table>', 1, '2019-09-10 04:36:07', NULL),
(161, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/service_packages/add-save', 'Add New Data Box Moving at Service Packages', '', 1, '2019-09-10 04:37:17', NULL),
(162, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/service_packages/add-save', 'Add New Data Pesticides Hunt at Service Packages', '', 1, '2019-09-10 04:39:13', NULL),
(163, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/menu_management/edit-save/11', 'Update data Featured at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>path</td><td>AdminSubCategoriesController@setFeatured</td><td>AdminServicePackagesController@setFeatured</td></tr><tr><td>parent_id</td><td>10</td><td></td></tr></tbody></table>', 1, '2019-09-10 04:39:50', NULL),
(164, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/services/add-save', 'Add New Data  at Services', '', 1, '2019-09-10 04:43:55', NULL),
(165, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/services/add-save', 'Add New Data  at Services', '', 1, '2019-09-10 04:44:23', NULL),
(166, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/services/add-save', 'Add New Data  at Services', '', 1, '2019-09-10 04:44:49', NULL),
(167, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', 'http://localhost:8000/admin/services/add-save', 'Add New Data  at Services', '', 1, '2019-09-10 04:45:10', NULL),
(168, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@breakbite.com login with IP Address 127.0.0.1', '', 1, '2019-10-17 09:19:03', NULL),
(169, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 'http://localhost:8000/admin/areas/add-save', 'Add New Data Dhanmondi at Area', '', 1, '2019-10-17 09:22:07', NULL),
(170, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 'http://localhost:8000/admin/areas/add-save', 'Add New Data Sukrabad at Area', '', 1, '2019-10-17 09:22:15', NULL),
(171, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 'http://localhost:8000/admin/module_generator/delete/19', 'Delete data City at Module Generator', '', 1, '2019-10-17 09:22:35', NULL),
(172, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 'http://localhost:8000/admin/cities/add-save', 'Add New Data Dhaka at City', '', 1, '2019-10-17 09:25:51', NULL),
(173, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 'http://localhost:8000/admin/cities/add-save', 'Add New Data Chittagong at City', '', 1, '2019-10-17 09:26:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_menus`
--

CREATE TABLE `cms_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'url',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_dashboard` tinyint(1) NOT NULL DEFAULT 0,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_menus`
--

INSERT INTO `cms_menus` (`id`, `name`, `type`, `path`, `color`, `icon`, `parent_id`, `is_active`, `is_dashboard`, `id_cms_privileges`, `sorting`, `created_at`, `updated_at`) VALUES
(2, 'Services', 'Route', 'AdminServicesControllerGetIndex', NULL, 'fa fa-globe', 0, 1, 0, 1, 3, '2019-05-27 23:30:00', NULL),
(3, 'Categories', 'Route', 'AdminCategoriesControllerGetIndex', NULL, 'fa fa-list', 0, 1, 0, 1, 2, '2019-05-29 00:07:47', NULL),
(4, 'HomeSlider', 'Route', 'AdminSliderControllerGetIndex', NULL, 'fa fa-sliders', 10, 1, 0, 1, 2, '2019-05-29 02:01:35', NULL),
(5, 'Service Packages', 'Module', 'service_packages', 'normal', 'fa fa-gift', 3, 1, 0, 1, 2, '2019-05-29 02:26:08', '2019-09-08 00:34:39'),
(7, 'Orders', 'Controller & Method', 'OrdersController@adminGetOrders', 'normal', 'fa fa-shopping-bag', 0, 1, 0, 1, 4, '2019-07-05 04:11:32', '2019-07-05 04:12:41'),
(8, 'Categories', 'Module', 'categories', 'normal', 'fa fa-list-ul', 3, 1, 0, 1, 1, '2019-09-07 22:23:11', '2019-09-07 22:25:01'),
(10, 'Catalog', 'URL', '#', 'normal', 'fa fa-th-list', 0, 1, 0, 1, 1, '2019-09-07 23:55:33', NULL),
(11, 'Featured', 'Controller & Method', 'AdminServicePackagesController@setFeatured', 'normal', 'fa fa-star-o', 10, 1, 0, 1, 1, '2019-09-08 00:00:36', '2019-09-10 04:39:50'),
(12, 'Area', 'Route', 'AdminAreasControllerGetIndex', NULL, 'fa fa-star', 0, 1, 0, 1, 5, '2019-10-17 09:19:26', NULL),
(14, 'City', 'Route', 'AdminCitiesControllerGetIndex', NULL, 'fa fa-gear', 0, 1, 0, 1, 6, '2019-10-17 09:25:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_menus_privileges`
--

CREATE TABLE `cms_menus_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_menus` int(11) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_menus_privileges`
--

INSERT INTO `cms_menus_privileges` (`id`, `id_cms_menus`, `id_cms_privileges`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(6, 6, 1),
(9, 7, 1),
(13, 8, 1),
(14, 9, 1),
(15, 10, 1),
(20, 5, 1),
(21, 11, 1),
(22, 12, 1),
(23, 13, 1),
(24, 14, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_moduls`
--

CREATE TABLE `cms_moduls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_moduls`
--

INSERT INTO `cms_moduls` (`id`, `name`, `icon`, `path`, `table_name`, `controller`, `is_protected`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Notifications', 'fa fa-cog', 'notifications', 'cms_notifications', 'NotificationsController', 1, 1, '2019-05-27 22:11:26', NULL, NULL),
(2, 'Privileges', 'fa fa-cog', 'privileges', 'cms_privileges', 'PrivilegesController', 1, 1, '2019-05-27 22:11:26', NULL, NULL),
(3, 'Privileges Roles', 'fa fa-cog', 'privileges_roles', 'cms_privileges_roles', 'PrivilegesRolesController', 1, 1, '2019-05-27 22:11:26', NULL, NULL),
(4, 'Users Management', 'fa fa-users', 'users', 'cms_users', 'AdminCmsUsersController', 0, 1, '2019-05-27 22:11:26', NULL, NULL),
(5, 'Settings', 'fa fa-cog', 'settings', 'cms_settings', 'SettingsController', 1, 1, '2019-05-27 22:11:26', NULL, NULL),
(6, 'Module Generator', 'fa fa-database', 'module_generator', 'cms_moduls', 'ModulsController', 1, 1, '2019-05-27 22:11:26', NULL, NULL),
(7, 'Menu Management', 'fa fa-bars', 'menu_management', 'cms_menus', 'MenusController', 1, 1, '2019-05-27 22:11:26', NULL, NULL),
(8, 'Email Templates', 'fa fa-envelope-o', 'email_templates', 'cms_email_templates', 'EmailTemplatesController', 1, 1, '2019-05-27 22:11:26', NULL, NULL),
(9, 'Statistic Builder', 'fa fa-dashboard', 'statistic_builder', 'cms_statistics', 'StatisticBuilderController', 1, 1, '2019-05-27 22:11:26', NULL, NULL),
(10, 'API Generator', 'fa fa-cloud-download', 'api_generator', '', 'ApiCustomController', 1, 1, '2019-05-27 22:11:26', NULL, NULL),
(11, 'Log User Access', 'fa fa-flag-o', 'logs', 'cms_logs', 'LogsController', 1, 1, '2019-05-27 22:11:26', NULL, NULL),
(12, 'Vendors', 'fa fa-product-hunt', 'vendors', 'vendors', 'AdminVendorsController', 0, 0, '2019-05-27 23:01:50', NULL, NULL),
(13, 'Services', 'fa fa-globe', 'services', 'services', 'AdminServicesController', 0, 0, '2019-05-27 23:30:00', NULL, NULL),
(14, 'Categories', 'fa fa-list', 'categories', 'categories', 'AdminCategoriesController', 0, 0, '2019-05-29 00:07:46', NULL, NULL),
(15, 'HomeSlider', 'fa fa-sliders', 'slider', 'slider', 'AdminSliderController', 0, 0, '2019-05-29 02:01:35', NULL, NULL),
(16, 'Service Packages', 'fa fa-list-ol', 'service_packages', 'service_packages', 'AdminServicePackagesController', 0, 0, '2019-05-29 02:26:07', NULL, NULL),
(17, 'Featured', 'fa fa-star', 'featured_sub_category', 'featured_sub_category', 'AdminFeaturedSubCategoryController', 0, 0, '2019-09-07 23:51:27', NULL, '2019-09-07 23:58:29'),
(18, 'Area', 'fa fa-star', 'areas', 'areas', 'AdminAreasController', 0, 0, '2019-10-17 09:19:26', NULL, NULL),
(19, 'City', 'fa fa-signal', 'cities', 'cities', 'AdminCitiesController', 0, 0, '2019-10-17 09:21:07', NULL, '2019-10-17 09:22:35'),
(20, 'City', 'fa fa-gear', 'cities', 'cities', 'AdminCitiesController', 0, 0, '2019-10-17 09:25:23', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_notifications`
--

CREATE TABLE `cms_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_users` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_privileges`
--

CREATE TABLE `cms_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_superadmin` tinyint(1) DEFAULT NULL,
  `theme_color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_privileges`
--

INSERT INTO `cms_privileges` (`id`, `name`, `is_superadmin`, `theme_color`, `created_at`, `updated_at`) VALUES
(1, 'BreakBite', 1, 'skin-blue-light', '2019-05-27 22:11:26', NULL),
(2, 'Vendor', 0, 'skin-blue-light', NULL, NULL),
(3, 'User', 0, 'skin-green-light', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_privileges_roles`
--

CREATE TABLE `cms_privileges_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_create` tinyint(1) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `is_edit` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `id_cms_moduls` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_privileges_roles`
--

INSERT INTO `cms_privileges_roles` (`id`, `is_visible`, `is_create`, `is_read`, `is_edit`, `is_delete`, `id_cms_privileges`, `id_cms_moduls`, `created_at`, `updated_at`) VALUES
(14, 1, 0, 1, 0, 0, 2, 13, NULL, NULL),
(18, 1, 0, 1, 0, 0, 3, 13, NULL, NULL),
(19, 1, 1, 1, 1, 1, 1, 14, NULL, NULL),
(20, 1, 1, 1, 1, 1, 1, 15, NULL, NULL),
(21, 1, 1, 1, 1, 1, 1, 13, NULL, NULL),
(22, 1, 1, 1, 1, 1, 1, 16, NULL, NULL),
(23, 1, 1, 1, 1, 1, 1, 4, NULL, NULL),
(24, 1, 1, 1, 1, 1, 1, 12, NULL, NULL),
(25, 1, 1, 1, 1, 1, 1, 17, NULL, NULL),
(26, 1, 1, 1, 1, 1, 1, 18, NULL, NULL),
(27, 1, 1, 1, 1, 1, 1, 19, NULL, NULL),
(28, 1, 1, 1, 1, 1, 1, 20, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_settings`
--

CREATE TABLE `cms_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_input_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dataenum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helper` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_settings`
--

INSERT INTO `cms_settings` (`id`, `name`, `content`, `content_input_type`, `dataenum`, `helper`, `created_at`, `updated_at`, `group_setting`, `label`) VALUES
(1, 'login_background_color', NULL, 'text', NULL, 'Input hexacode', '2019-05-27 22:11:27', NULL, 'Login Register Style', 'Login Background Color'),
(2, 'login_font_color', NULL, 'text', NULL, 'Input hexacode', '2019-05-27 22:11:27', NULL, 'Login Register Style', 'Login Font Color'),
(3, 'login_background_image', NULL, 'upload_image', NULL, NULL, '2019-05-27 22:11:27', NULL, 'Login Register Style', 'Login Background Image'),
(4, 'email_sender', 'support@crudbooster.com', 'text', NULL, NULL, '2019-05-27 22:11:27', NULL, 'Email Setting', 'Email Sender'),
(5, 'smtp_driver', 'mail', 'select', 'smtp,mail,sendmail', NULL, '2019-05-27 22:11:27', NULL, 'Email Setting', 'Mail Driver'),
(6, 'smtp_host', '', 'text', NULL, NULL, '2019-05-27 22:11:27', NULL, 'Email Setting', 'SMTP Host'),
(7, 'smtp_port', '25', 'text', NULL, 'default 25', '2019-05-27 22:11:27', NULL, 'Email Setting', 'SMTP Port'),
(8, 'smtp_username', '', 'text', NULL, NULL, '2019-05-27 22:11:27', NULL, 'Email Setting', 'SMTP Username'),
(9, 'smtp_password', '', 'text', NULL, NULL, '2019-05-27 22:11:27', NULL, 'Email Setting', 'SMTP Password'),
(10, 'appname', 'CRUDBooster', 'text', NULL, NULL, '2019-05-27 22:11:27', NULL, 'Application Setting', 'Application Name'),
(11, 'default_paper_size', 'Legal', 'text', NULL, 'Paper size, ex : A4, Legal, etc', '2019-05-27 22:11:27', NULL, 'Application Setting', 'Default Paper Print Size'),
(12, 'logo', '', 'upload_image', NULL, NULL, '2019-05-27 22:11:27', NULL, 'Application Setting', 'Logo'),
(13, 'favicon', '', 'upload_image', NULL, NULL, '2019-05-27 22:11:27', NULL, 'Application Setting', 'Favicon'),
(14, 'api_debug_mode', 'true', 'select', 'true,false', NULL, '2019-05-27 22:11:27', NULL, 'Application Setting', 'API Debug Mode'),
(15, 'google_api_key', '', 'text', NULL, NULL, '2019-05-27 22:11:27', NULL, 'Application Setting', 'Google API Key'),
(16, 'google_fcm_key', '', 'text', NULL, NULL, '2019-05-27 22:11:27', NULL, 'Application Setting', 'Google FCM Key');

-- --------------------------------------------------------

--
-- Table structure for table `cms_statistics`
--

CREATE TABLE `cms_statistics` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_statistic_components`
--

CREATE TABLE `cms_statistic_components` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_statistics` int(11) DEFAULT NULL,
  `componentID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `component_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_users`
--

CREATE TABLE `cms_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_users`
--

INSERT INTO `cms_users` (`id`, `name`, `photo`, `email`, `password`, `id_cms_privileges`, `remember_token`, `created_at`, `updated_at`, `status`) VALUES
(1, 'BreakBite Tech Team', 'uploads/1/2019-07/292fd6b59e.png', 'admin@breakbite.com', '$2y$10$P6O2uN6ZWwjQ1mdOppZ6Re8uLJRVfqUB5rxphpoFvBcH1XIMamJky', 1, NULL, '2019-05-27 22:11:26', '2019-07-05 04:25:24', 'Active'),
(2, 'Rakesh Ahmed', NULL, 'faisal14gram1@gmail.com', '$2y$10$mDYyu01rNv.FFA0tnfR2FeCtopbXBy5t97G/JshGUm7cXzSZlP7EC', 2, 'PTkZqN1Rym7l5HcfXp2NAKzz8cH4OliS72YMp2VsIcm89QEFaOWKUqKYwu4M', '2019-10-17 09:11:42', '2019-10-17 09:11:42', NULL),
(3, 'Md Foysal', NULL, 'faisal14gram2@gmail.com', '$2y$10$6u4qS7sumEw.cJb4CLnWNei7BuxsK4osUtmQ66t9tjsUweRMtgEL.', 2, 'fZrHxs5FVLjSQPFc3L3NKY8wjr72F0CF0isnaq237f7YKwb6SDVw4Uagw1I2', '2019-10-26 03:29:55', '2019-10-26 03:29:55', NULL),
(4, 'Rakesh Ahmed', NULL, 'faisal14gram1234@gmail.com', '$2y$10$u8yhiUY9E83N5ueA3LYYWOm7T44DQVL4uDGZEqULqQt60nMWPJ56O', 2, 'zChrfeN1Aaojvtmb8BINRBQnNuBPRBwxglqGBhsnDEDTNSspiNHRioE3Slvf', '2019-10-26 04:43:35', '2019-10-26 04:43:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `featured_packages`
--

CREATE TABLE `featured_packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `packages_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `featured_packages`
--

INSERT INTO `featured_packages` (`id`, `created_at`, `updated_at`, `packages_id`) VALUES
(1, NULL, NULL, 1),
(2, NULL, NULL, 2),
(3, NULL, NULL, 3),
(4, NULL, NULL, 4);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_08_07_145904_add_table_cms_apicustom', 1),
(2, '2016_08_07_150834_add_table_cms_dashboard', 1),
(3, '2016_08_07_151210_add_table_cms_logs', 1),
(4, '2016_08_07_151211_add_details_cms_logs', 1),
(5, '2016_08_07_152014_add_table_cms_privileges', 1),
(6, '2016_08_07_152214_add_table_cms_privileges_roles', 1),
(7, '2016_08_07_152320_add_table_cms_settings', 1),
(8, '2016_08_07_152421_add_table_cms_users', 1),
(9, '2016_08_07_154624_add_table_cms_menus_privileges', 1),
(10, '2016_08_07_154624_add_table_cms_moduls', 1),
(11, '2016_08_17_225409_add_status_cms_users', 1),
(12, '2016_08_20_125418_add_table_cms_notifications', 1),
(13, '2016_09_04_033706_add_table_cms_email_queues', 1),
(14, '2016_09_16_035347_add_group_setting', 1),
(15, '2016_09_16_045425_add_label_setting', 1),
(16, '2016_09_17_104728_create_nullable_cms_apicustom', 1),
(17, '2016_10_01_141740_add_method_type_apicustom', 1),
(18, '2016_10_01_141846_add_parameters_apicustom', 1),
(19, '2016_10_01_141934_add_responses_apicustom', 1),
(20, '2016_10_01_144826_add_table_apikey', 1),
(21, '2016_11_14_141657_create_cms_menus', 1),
(22, '2016_11_15_132350_create_cms_email_templates', 1),
(23, '2016_11_15_190410_create_cms_statistics', 1),
(24, '2016_11_17_102740_create_cms_statistic_components', 1),
(25, '2017_06_06_164501_add_deleted_at_cms_moduls', 1),
(26, '2019_05_28_045559_create_vendors_table', 2),
(27, '2019_05_28_052607_create_services_table', 3),
(28, '2019_05_29_034600_create_users_table', 4),
(29, '2019_05_29_060344_create_categories_table', 5),
(30, '2019_05_29_080018_create_slider_table', 6),
(31, '2019_05_29_082337_create_sub_categories_table', 7),
(32, '2019_05_29_133304_create_vendors_providing_table', 8),
(33, '2019_06_24_181331_create_cart_table', 9),
(34, '2019_06_24_185329_create_order_table', 10),
(35, '2019_09_08_051514_cteate_featured_sub_category', 11),
(36, '2019_10_17_151728_create_cities_table', 12),
(37, '2019_10_17_151741_create_areas_table', 12),
(38, '2019_10_17_153548_create_service_locations_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `services_id` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `users_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `service_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `packages_id` int(11) DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_fixed` int(11) DEFAULT 0,
  `price` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_range` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `created_at`, `updated_at`, `service_name`, `packages_id`, `description`, `image`, `price_fixed`, `price`, `price_range`) VALUES
(1, '2019-09-08 23:15:55', '2019-09-08 23:18:22', 'Planting Avocado', 2, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque \r\nquis felis mollis, tempor metus eget, imperdiet lacus. Aliquam nulla \r\nneque, mollis sit amet dolor eget, porta lacinia leo. Ut ultrices \r\ntincidunt eleifend. Aenean leo ipsum, vehicula ac elementum at, \r\npellentesque at nunc. Mauris pellentesque lacus posuere mauris tristique\r\n accumsan. Quisque diam erat, porttitor a finibus id, posuere eget \r\nligula. Morbi vitae blandit odio. Donec volutpat ligula dolor. Vivamus \r\nvarius, nunc vel vehicula blandit, ipsum lectus gravida arcu, id \r\nbibendum metus ex quis urna. Nunc a varius neque, vitae faucibus tortor.\r\n Phasellus dolor sem, lobortis at odio at, convallis feugiat erat. Nam \r\nquam lacus, placerat eu hendrerit ut, porta sed elit. Ut vitae ornare \r\njusto, sit amet feugiat felis. Suspendisse euismod at leo sed auctor. \r\nDuis at quam et justo sagittis consequat.</p>', 'uploads/1/2019-09/pexels_photo_24311_600x600.jpg', 0, '120', NULL),
(2, '2019-09-10 04:43:55', NULL, 'Moving Box Delivery', 3, '<p>wewevew<br></p>', 'uploads/1/2019-09/pexels_photo_24325_600x600.jpg', 0, '80', NULL),
(3, '2019-09-10 04:44:23', NULL, 'Packing Services', 3, '<p>wvdwvwvwv<br></p>', 'uploads/1/2019-09/pexels_photo_111150_600x600.jpeg', 0, '40', NULL),
(4, '2019-09-10 04:44:49', NULL, 'Truck for 1 day', 3, '<p>aebebs<br></p>', 'uploads/1/2019-09/pexels_photo_24619_600x600.jpg', 0, '130', NULL),
(5, '2019-09-10 04:45:10', NULL, 'Truck for 3 day', 3, '<p>awgewaww<br></p>', 'uploads/1/2019-09/pexels_photo_25036_600x600.jpg', 0, '300', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_locations`
--

CREATE TABLE `service_locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_locations`
--

INSERT INTO `service_locations` (`id`, `name`, `phone`, `city_id`, `area_id`, `address`, `created_at`, `updated_at`) VALUES
(1, 'Md Foysal', '01832970231', 2, 2, 'nurjahan road', '2019-10-17 09:58:42', NULL),
(2, 'Md Foysal', '01832970231', 1, 1, 'nurjahan road', '2019-10-17 10:00:00', NULL),
(3, 'Rakesh Ahmed', '01832970231', 2, 2, '14/10,DHAKA', '2019-10-26 00:43:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_packages`
--

CREATE TABLE `service_packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `package_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_heading` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_packages`
--

INSERT INTO `service_packages` (`id`, `created_at`, `updated_at`, `package_name`, `categories_id`, `image`, `sub_heading`, `description`) VALUES
(1, '2019-09-08 00:20:34', '2019-09-10 04:36:07', 'Gardening', '3', 'uploads/1/2019-09/salad_water_garden_plant.jpg', 'cement, plastic solvent, caulking, or soldering, brazing and welding equipment. I can fill pipes or plumbing fixtures with water or air', '<p><br></p>'),
(2, '2019-09-08 01:38:52', '2019-09-10 04:35:01', 'Plumbing Services', '5', 'uploads/1/2019-09/plumbing_1433606_960_720.jpg', 'Installing, repairing and maintaining pipes, fixtures and other plumbing', '<p style=\"font-family: Verdana, &quot;Open Sans&quot;, sans-serif; margin-bottom: 24px; overflow-wrap: break-word; color: rgb(96, 96, 96); background-color: rgb(242, 242, 242);\">I can assemble pipe sections, tubing and fittings, using couplings, clamps, screws, bolts, cement, plastic solvent, caulking, or soldering, brazing and welding equipment. I can fill pipes or plumbing fixtures with water or air and observe pressure gauges to detect and locate leaks.</p><p style=\"font-family: Verdana, &quot;Open Sans&quot;, sans-serif; margin-bottom: 24px; overflow-wrap: break-word; color: rgb(96, 96, 96); background-color: rgb(242, 242, 242);\">I can also Review blueprints and building codes and specifications to determine work details and procedures. I can prepare written work cost estimates and negotiate contracts and keep records of assignments and produce detailed work reports.</p>'),
(3, '2019-09-10 04:37:17', NULL, 'Box Moving', '6', 'uploads/1/2019-09/pexels_photo_24325.jpg', 'Moving Furniture and Goods , Packing Services', '<p>I can load and unload furniture and related materials onto and off \r\ntrucks, ensuring that appropriate padding and ropes are used to prevent \r\ndamage. <br></p>'),
(4, '2019-09-10 04:39:13', NULL, 'Pesticides Hunt', '6', 'uploads/1/2019-09/pexels_photo_53813.jpeg', 'Roaches, rats, mice, spiders, termites, fleas, ants, and bees', '<p>I can use two different types of pesticides—general use and restricted \r\nuse. General use pesticides are the most widely used and are readily </p>');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `created_at`, `updated_at`, `image`) VALUES
(12, '2019-07-04 14:32:43', '2019-09-07 10:53:19', 'uploads/1/2019-09/tool_work_bench_hammer_pliers_53987.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `created_at`, `updated_at`, `company`, `location`, `fname`, `lname`) VALUES
(1, '2019-05-27 23:24:03', NULL, 'Shakil\'s Shop', 'Bashundhara City, Dhaka', 'Shakil', 'Anwar'),
(2, NULL, NULL, 'Test Service Provider', 'Dhaka', 'Test', 'TEst');

-- --------------------------------------------------------

--
-- Table structure for table `vendors_providing`
--

CREATE TABLE `vendors_providing` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `vendors_id` int(11) NOT NULL,
  `services_id` int(11) NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_range` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendors_providing`
--

INSERT INTO `vendors_providing` (`id`, `created_at`, `updated_at`, `vendors_id`, `services_id`, `price`, `price_range`, `description`) VALUES
(11, NULL, NULL, 1, 3, '1200', NULL, 'Nothing'),
(12, NULL, NULL, 1, 1, '900', NULL, 'Test'),
(13, NULL, NULL, 1, 2, NULL, '900-1000', 'Test');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_apikey`
--
ALTER TABLE `cms_apikey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_logs`
--
ALTER TABLE `cms_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_menus`
--
ALTER TABLE `cms_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_moduls`
--
ALTER TABLE `cms_moduls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_notifications`
--
ALTER TABLE `cms_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_privileges`
--
ALTER TABLE `cms_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_settings`
--
ALTER TABLE `cms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_statistics`
--
ALTER TABLE `cms_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_users`
--
ALTER TABLE `cms_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `featured_packages`
--
ALTER TABLE `featured_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_locations`
--
ALTER TABLE `service_locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_packages`
--
ALTER TABLE `service_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors_providing`
--
ALTER TABLE `vendors_providing`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_apikey`
--
ALTER TABLE `cms_apikey`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_logs`
--
ALTER TABLE `cms_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;

--
-- AUTO_INCREMENT for table `cms_menus`
--
ALTER TABLE `cms_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `cms_moduls`
--
ALTER TABLE `cms_moduls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `cms_notifications`
--
ALTER TABLE `cms_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_privileges`
--
ALTER TABLE `cms_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `cms_settings`
--
ALTER TABLE `cms_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cms_statistics`
--
ALTER TABLE `cms_statistics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_users`
--
ALTER TABLE `cms_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `featured_packages`
--
ALTER TABLE `featured_packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `service_locations`
--
ALTER TABLE `service_locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `service_packages`
--
ALTER TABLE `service_packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vendors_providing`
--
ALTER TABLE `vendors_providing`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
