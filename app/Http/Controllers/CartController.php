<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class CartController extends Controller
{
    public function get_cart()
    {
      $id = Auth::Id();
      $cart = DB::table('cart')
      ->join('services','services.id','=','cart.services_id')
      ->select('cart.id','title','services.price','services_id','users_id','sub_categories_id','description','image','price_fixed','price_range')
      ->where('users_id',$id)
      ->get();

      $count = DB::table('cart')
      ->join('services','services.id','=','cart.services_id')
      ->select('cart.id','title','services.price','services_id','users_id','sub_categories_id','description','image','price_fixed','price_range')
      ->where('users_id',$id)
      ->count();
      
      return view('cart',compact('cart','count'));
    }

    public function remove_cart()
    {
      $id = $_REQUEST["item"];
      DB::table('cart')
      ->where('id',$id)
      ->delete();
    }

    public function checkout()
    {

    }
}
