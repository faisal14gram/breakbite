<?php

namespace App\Http\Controllers;
use \Shipu\SslWPayment\Payment;
use Auth;
use DB;

use Illuminate\Http\Request;

class PaymentsController extends Controller
{
	public function payment(){

	$payment = new Payment(config('sslwpayment'));
    return $payment->paymentUrl();
	}

	public function hiddenInput(){

	$payment = new Payment(config('sslwpayment'));

    return $payment->customer([
    'cus_name'  => 'Shipu Ahamed', // Customer name
    'cus_email' => 'shipuahamed01@gmail.com', // Customer email
    'cus_phone' => '01616022669' // Customer Phone
    ])->transactionId('21005455540')->amount(3500)->hiddenValue();

	}

	public function transactionId(){

	$payment = new Payment(config('sslwpayment'));
    return $payment->generateTransaction();

	}

	public function checkValidResponse(){

	 $payment = new Payment(config('sslwpayment'));
     return $payment->valid($request);
	}

	public function checkValidResponseAmount(){

	 $payment = new Payment(config('sslwpayment'));
     return $payment->valid($request, '3500'); 
	}

	public function checkValidResponseId(){

	$payment = new Payment(config('sslwpayment'));
	return $payment->valid($request, '3500', '21005455540');
	}


	public function paymentSuccessOrFailed(Request $request){

    if($request->get('status') == 'CANCELLED') {
        return redirect()->back();
    }
    
    $transactionId = $request->get('tran_id');
    $valid = Payment::valid($request, 3500, $transactionId);
    
    if($valid) {
        echo "Successfully Paid";
    } else {
    	 echo "Something went wrong";
       
    }
    
    return redirect()->back();
}

 public function paymentblade(){
 	$user=Auth::user();
 	$amount = DB::table('cart')->where('users_id',$user->id)->sum('price');
 	$payment = new Payment(config('sslwpayment'));
    $id=$payment->generateTransaction();
  
    // dd($user);
   return view('hiddeninput',compact('id','user','amount'));
   }


   public function paymentSuccess(){

   	echo "Success";
   }


   public function paymentFailed(){

   	echo "Failed";
   }

   public function paymentCancel(){

   	echo "Canceled";
   }
}
