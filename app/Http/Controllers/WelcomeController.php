<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class WelcomeController extends Controller
{
    public function index()
    {
      $categories = DB::table('categories')
                    ->get();

      $featured = DB::Table('featured_packages')
                  ->join('service_packages','service_packages.id','=','featured_packages.packages_id')
                  ->get();
      return view('welcome',compact('categories','featured'));
    }
}
