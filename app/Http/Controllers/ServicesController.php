<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\City;
use App\Area;
use Carbon\Carbon;
use App\ServiceLocation;


class ServicesController extends Controller
{
  public function getPack($id)
  {
    $package = DB::table('service_packages')
              ->where('id',$id)
              ->first();
    $services = DB::table('services')
              ->where('packages_id',$id)
              ->get();
    return view('package_profile',compact('package','services'));
  }

  public function addCart()
  {
    $service = $_REQUEST['services_id'];
    $user=Auth::user()->id;

    $get_service = DB::table('services')
                  ->where('id',$service)
                  ->first();

    $values = array('users_id'=>$user,'services_id'=>$service,'price'=>$get_service->price);

      DB::table('cart')
      ->insert($values);
  }

  public function getCategoryPackages($category_id)
  {
    $packages = DB::table('service_packages')
              ->where('categories_id',$category_id)
              ->get();
    $category = DB::table('categories')
              ->where('id',$category_id)
              ->first();
    return view('packages',compact('packages','category'));
  }

  public function getCart()
  {
    $user=Auth::user()->id;
    $cart = DB::table('cart')
          ->select('cart.id','services.service_name','services.image','services.price')
          ->join('services','cart.services_id','=','services.id')
          ->where('users_id',$user)
          ->get();
    $count = DB::table('cart')
    ->count();


      $cities=City::orderBy('id','desc')->get();

      $areas=Area::orderBy('id','desc')->get();


    return view('cart',compact('cart','count','cities','areas'));
  }


  public function locationStore(Request $request){
 
     $insert=ServiceLocation::insert([
            'name'=>$_POST['name'],
            'phone'=>$_POST['phone'],
            'city_id'=>$_POST['city'],
            'address'=>$_POST['address'],
            'area_id'=>$_POST['area'],
            'created_at'=>Carbon::now()->toDateTimeString(),
        ]);
     
     if($insert){
      return redirect('cart');
     }


  }




  public function deleteCart()
  {
    $id = $_REQUEST["item"];
    DB::table('cart')
    ->where('id',$id)
    ->delete();
  }
}
