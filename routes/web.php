<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//ADMIN START
Route::get('admin/set-featured','AdminServicePackagesController@setFeatured');
//ADMIN_END

//PACKAGE
Route::get('packages/{id}','ServicesController@getCategoryPackages');
Route::get('package-details/{id}','ServicesController@getPack');
//PACKAGE END

//CART
Route::get('cart/','ServicesController@getCart');
Route::post('service/location/store','ServicesController@locationStore');
Route::get('add_cart/','ServicesController@addCart');
Route::get('cart/delete','ServicesController@deleteCart');

//CART END

//PAYMENTS MESSAGE
Route::post('payment/success', 'PaymentsController@paymentSuccess')->name('payment.success');
Route::post('payment/failed', 'PaymentsController@paymentFailed')->name('payment.failed');
Route::post('payment/cancel', 'PaymentsController@paymentCancel')->name('payment.cancel');
Route::get('payment/view', 'PaymentsController@paymentblade')->name('payment.view');
